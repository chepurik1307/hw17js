const student = {
    name: '',
    lastName: '',
    tabel: {},
  };
  
  student.name = prompt('Введіть ім\'я студента', '');
  student.lastName = prompt('Введіть прізвище студента', '');
  
  let subjectName = prompt('Введіть назву предмета', '');
  while (subjectName !== null && subjectName !== '') {
    const score = parseFloat(prompt(`Введіть оцінку з предмета "${subjectName}"`, ''));
    if (!isNaN(score)) {
      student.tabel[subjectName] = score;
    }
    subjectName = prompt('Введіть назву предмета', '');
  }
  
  let badScoreCount = 0;
  let totalScore = 0;
  let scoresCount = 0;
  
  for (let subject in student.tabel) {
    const score = student.tabel[subject];
    if (score < 4) {
      badScoreCount++;
    }
    totalScore += score;
    scoresCount++;
  }
  
  if (badScoreCount === 0) {
    console.log('Студент переведено на наступний курс');
  }
  
  if (scoresCount > 0) {
    const averageScore = totalScore / scoresCount;
    console.log(`Середній бал: ${averageScore}`);
    if (averageScore > 7) {
      console.log('Студенту призначено стипендію');
    }
  }
  